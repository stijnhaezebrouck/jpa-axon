package be.vdab.axon.domain.aggregate;

import org.junit.Test;

import java.util.UUID;

import static be.vdab.axon.JsonUtil.json2object;
import static be.vdab.axon.JsonUtil.object2json;
import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationAggregateSerializationTest {

    @Test
    public void jsonSerialization() {
        ApplicationAggregate original = createInstance();
        ApplicationAggregate deserialized = serializeDeserialize(original);
        assertEqual(original, deserialized);
    }

    protected void assertEqual(ApplicationAggregate original, ApplicationAggregate deserialized) {
        assertThat(deserialized).isEqualToComparingFieldByFieldRecursively(original);
    }

    protected ApplicationAggregate createInstance() {
        return new ApplicationAggregate(UUID.randomUUID(), 42);
    }

    protected ApplicationAggregate serializeDeserialize(ApplicationAggregate object) {
        String serialized = object2json(object);
        return json2object(serialized, object.getClass());
    }
}

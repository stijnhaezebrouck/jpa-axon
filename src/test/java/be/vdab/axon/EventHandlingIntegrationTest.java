package be.vdab.axon;

import be.vdab.axon.domain.aggregate.AggregateCreatedEvent;
import be.vdab.axon.domain.aggregate.CreateAggregateCommand;
import be.vdab.axon.domain.aggregate.ThrowExceptionDuringCommandHandler;
import be.vdab.axon.domain.aggregate.ThrowExceptionDuringEventHandlerWithNewTransactionCommand;
import be.vdab.axon.domain.aggregate.ThrowExceptionDuringEventHandlerWithParticipatingTransactionCommand;
import be.vdab.axon.domain.aggregate.ThrowExceptionDuringEventHandlerWithoutTransactionCommand;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringCommandHandlerEvent;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringEventHandlerWithNewTransactionEvent;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringEventHandlerWithParticipatingTransactionEvent;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringEventHandlerWithoutTransactionEvent;
import org.junit.Test;
import org.springframework.transaction.TransactionSystemException;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;


public class EventHandlingIntegrationTest extends IntegrationTest {

    @Test
    public void createAggreate_addsToView() {
        CreateAggregateCommand create = new CreateAggregateCommand();
        commandGateway.sendAndWait(create);
        assertEventExistsInView(create.getAggregateId(), AggregateCreatedEvent.class);
    }

    @Test
    public void errorInCommandHandler_shouldRollback() {
        UUID id = givenAggregateCreated();
        try {
            commandGateway.sendAndWait(new ThrowExceptionDuringCommandHandler(id));
            fail("expected exception thrown");
        } catch (ApplicationException exc) {}

        assertEventExistsInEventStore(id, AggregateCreatedEvent.class);
        assertEventDoesNotExistInEventStore(id, ThrowedExceptionDuringCommandHandlerEvent.class);
        assertThat(numberOfEventsInEventStore(id)).isEqualTo(1);
        assertEventDoesNotExistInView(id, ThrowedExceptionDuringCommandHandlerEvent.class);

    }

    @Test
    public void errorInEventHandlerWithNewTransaction_shouldRollbackViewButNotAggregate() {
        UUID id = givenAggregateCreated();

        commandGateway.sendAndWait(new ThrowExceptionDuringEventHandlerWithNewTransactionCommand(id));

        assertThat(numberOfEventsInEventStore(id)).isEqualTo(2);
        assertEventDoesNotExistInView(id, ThrowedExceptionDuringEventHandlerWithNewTransactionEvent.class);
    }

    @Test
    public void errorInEventHandlerWithoutTransaction_shouldRollbackNothing() {
        UUID id = givenAggregateCreated();

        commandGateway.sendAndWait(new ThrowExceptionDuringEventHandlerWithoutTransactionCommand(id));

        assertThat(numberOfEventsInEventStore(id)).isEqualTo(2);
        assertEventExistsInView(id, ThrowedExceptionDuringEventHandlerWithoutTransactionEvent.class);
    }

    @Test
    public void errorInEventHandlerWithParticipatingTransaction_shouldRollbackAll() {
        UUID id = givenAggregateCreated();

        try {
            commandGateway.sendAndWait(new ThrowExceptionDuringEventHandlerWithParticipatingTransactionCommand(id));
            fail("expected exception here");
        } catch(TransactionSystemException exc) {}

        assertThat(numberOfEventsInEventStore(id)).isEqualTo(1);
        assertEventDoesNotExistInView(id, ThrowedExceptionDuringEventHandlerWithParticipatingTransactionEvent.class);
    }

    @Test
    public void eventSerialized_shouldBeSerializedInReadableJson() {
        UUID id = givenAggregateCreated();

        String payload = getDomainEventPayloadForFirstEvent(id);
        assertThat(payload).isEqualTo(JsonUtil.object2json(new AggregateCreatedEvent(id)));
    }
}

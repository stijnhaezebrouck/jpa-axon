package be.vdab.axon;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonUtil {

    private JsonUtil() {
        throw new UnsupportedOperationException("utility class");
    }

    public static String object2json(Object o) {
        try {
            ObjectMapper om = new ObjectMapper();
            return om.writeValueAsString(o);
        } catch(JsonProcessingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static <T> T json2object(String json, Class<T> parseClass) {
        try {
            ObjectMapper om = new ObjectMapper();
            return om.readValue(json, parseClass);
        }
        catch(IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}

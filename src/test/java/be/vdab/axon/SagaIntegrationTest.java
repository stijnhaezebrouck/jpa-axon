package be.vdab.axon;

import be.vdab.axon.domain.aggregate.AggregateCreatedEvent;
import be.vdab.axon.domain.aggregate.DoNothingCommand;
import be.vdab.axon.domain.aggregate.NothingDoneEvent;
import be.vdab.axon.domain.aggregate.RequestCommandFromSagaEvent;
import be.vdab.axon.domain.aggregate.SagaExpiredEvent;
import be.vdab.axon.domain.aggregate.SagaStartedEvent;
import be.vdab.axon.domain.aggregate.StartSagaCommand;
import be.vdab.axon.domain.aggregate.ThrowExceptionDuringCommandHandler;
import be.vdab.axon.domain.aggregate.ThrowSagaExceptionCommand;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringCommandHandlerEvent;
import be.vdab.axon.domain.aggregate.TriggerACommandFromSagaCommand;
import org.junit.Test;

import java.util.UUID;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class SagaIntegrationTest extends IntegrationTest {

    @Test
    public void startSaga() {
        UUID id = givenAggregateCreated();
        commandGateway.sendAndWait(new StartSagaCommand(id));
        assertThat(numberOfSagaEntries(id)).isEqualTo(1);
    }

    @Test
    public void throwExceptionInSaga() {
        UUID id = givenAggregateCreated();
        commandGateway.sendAndWait(new StartSagaCommand(id));
        commandGateway.sendAndWait(new ThrowSagaExceptionCommand(id));
        assertThat(numberOfSagaEntries(id)).isEqualTo(1);
    }

    @Test
    public void triggerCommandFromSaga() {
        UUID id = givenAggregateCreated();
        commandGateway.sendAndWait(new StartSagaCommand(id));
        commandGateway.sendAndWait(new TriggerACommandFromSagaCommand(id, DoNothingCommand.class));
        assertEventExistsInView(id, NothingDoneEvent.class);
    }

    @Test
    public void triggerFailingCommandFromSaga() {
        UUID id = givenAggregateCreated();
        commandGateway.sendAndWait(new StartSagaCommand(id));
        commandGateway.sendAndWait(new TriggerACommandFromSagaCommand(id, ThrowExceptionDuringCommandHandler.class));

        assertEventExistsInEventStore(id, AggregateCreatedEvent.class);
        assertEventExistsInEventStore(id, SagaStartedEvent.class);
        assertEventExistsInEventStore(id, RequestCommandFromSagaEvent.class);
        assertEventDoesNotExistInEventStore(id, ThrowedExceptionDuringCommandHandlerEvent.class);

        assertEventExistsInView(id, AggregateCreatedEvent.class);
        assertEventExistsInView(id, SagaStartedEvent.class);
        assertEventExistsInView(id, RequestCommandFromSagaEvent.class);
        assertEventDoesNotExistInView(id, ThrowedExceptionDuringCommandHandlerEvent.class);
    }

    @Test
    public void sagaExpiredTest() throws InterruptedException {
        int numberOfEventsInEventStoreBeforeTest = numberOfEventsInEventStore(SagaExpiredEvent.class);
        UUID id = givenAggregateCreated();
        commandGateway.sendAndWait(new StartSagaCommand(id));
        assertThat(numberOfSagaEntries(id)).isEqualTo(1);
        await().atMost(30, SECONDS)
                .until(()-> numberOfEventsInView(id, SagaExpiredEvent.class) == 1);
        assertThat(numberOfSagaEntries(id)).isEqualTo(0);
        assertEventExistsInView(id, SagaExpiredEvent.class);

        //The expiry event does not originate from Aggregate, so it has its own UUID, and its type is null
        assertEventDoesNotExistInEventStore(id, SagaExpiredEvent.class);
        assertThat(numberOfEventsInEventStore(SagaExpiredEvent.class)).isGreaterThan(numberOfEventsInEventStoreBeforeTest);
    }

    private int numberOfSagaEntries(UUID aggregateId) {
        return jdbc.queryForObject("select count(*) from " + schema + ".association_value_entry where association_value_entry.ASSOCIATION_VALUE=?",
                new Object[] {aggregateId.toString()},
                int.class);
    }

}

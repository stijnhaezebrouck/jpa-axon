package be.vdab.axon;

import be.vdab.axon.domain.aggregate.ApplicationAggregate;
import be.vdab.axon.domain.aggregate.DoNothingCommand;
import be.vdab.axon.infrastructure.AxonConfig;
import be.vdab.axon.infrastructure.JsonSnapshotEventEntry;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.waitAtMost;
import static org.mockito.internal.util.reflection.Whitebox.getInternalState;

public class SnapshottingIntegrationTest extends IntegrationTest {

    @Autowired
    private AxonConfig axonConfig;

    @Test
    public void snapshot_shouldBeSerializedInReadableJson() {
        assertThat(axonConfig.snapshotThreshold() > 2).isTrue();

        UUID id = givenAggregateCreated();

        for(int i = 0; i < axonConfig.snapshotThreshold() +10 ; i++) {
            additionalCommand(id);
        }

        waitUntilSnapshotIsCreated(id);

        String serializedPayload = getSnapshotPayload(id);

        ApplicationAggregate deserializedPayload = JsonUtil.json2object(serializedPayload, ApplicationAggregate.class);
        assertThat(getInternalState(deserializedPayload, "id" )).isEqualTo(id);
    }

    private void additionalCommand(UUID id) {
        commandGateway.sendAndWait(new DoNothingCommand(id));
    }


    private void waitUntilSnapshotIsCreated(UUID id) {
        waitAtMost(10, SECONDS).until(() -> getFirstPayload(JsonSnapshotEventEntry.TABLE_NAME, id).isPresent());
    }

    private String getSnapshotPayload(UUID id) {
        return getFirstPayload(JsonSnapshotEventEntry.TABLE_NAME, id)
                .orElseThrow(() -> new RuntimeException("no snapshot event found for " + id));
    }

}

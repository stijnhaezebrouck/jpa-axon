package be.vdab.axon;

import be.vdab.axon.domain.aggregate.CreateAggregateCommand;
import be.vdab.axon.infrastructure.JsonDomainEventEntry;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class IntegrationTest {

    @Autowired
    protected CommandGateway commandGateway;

    @Autowired
    protected JdbcTemplate jdbc;

    @Value("${spring.datasource.properties.hibernate.default_schema}")
    protected String schema;


    protected UUID givenAggregateCreated() {
        CreateAggregateCommand create = new CreateAggregateCommand();
        commandGateway.sendAndWait(create);
        return create.getAggregateId();
    }

    protected int numberOfEventsInEventStore(Class<? extends Event> eventClass) {
        return jdbc.queryForObject("select count(*) from " + schema + ".domain_event_entry where payload_type=?",
                new Object[] {eventClass.getName()},
                int.class);
    }

    protected void assertEventExistsInView(UUID aggregateId, Class<?> eventClass) {
        assertThat(numberOfEventsInView(aggregateId, eventClass)).isEqualTo(1);
    }

    protected void assertEventDoesNotExistInView(UUID aggregateId, Class<?> eventClass) {
        assertThat(numberOfEventsInView(aggregateId, eventClass)).isEqualTo(0);
    }

    protected int numberOfEventsInEventStore(UUID aggregateId) {
        return jdbc.queryForObject("select count(*) from " + schema + ".domain_event_entry where aggregate_identifier=?",
                new Object[] {aggregateId.toString()},
                int.class);
    }

    protected int numberOfEventsInEventStore(UUID aggregateId, Class<? extends Event> eventClass) {
        return jdbc.queryForObject("select count(*) from " + schema + ".domain_event_entry where aggregate_identifier=? and payload_type=?",
                new Object[] {aggregateId.toString(), eventClass.getName()},
                int.class);
    }

    protected int numberOfEventsInView(UUID aggregateId, Class<?> eventClass) {
        return jdbc.queryForObject("select count(*) from " + schema + ".event_list_view where aggregateid=? and eventname=?",
                new Object[] {aggregateId.toString(), eventClass.getSimpleName()},
                int.class);
    }

    protected void assertEventExistsInEventStore(UUID aggregateId, Class<? extends Event> eventType) {
        assertThat(numberOfEventsInEventStore(aggregateId, eventType)).isEqualTo(1);
    }

    protected void assertEventDoesNotExistInEventStore(UUID aggregateId, Class<? extends Event> eventType) {
        assertThat(numberOfEventsInEventStore(aggregateId, eventType)).isEqualTo(0);
    }

    protected String getDomainEventPayloadForFirstEvent(UUID aggregateId) {
        return getFirstPayload(JsonDomainEventEntry.TABLE_NAME, aggregateId)
                .orElseThrow(()-> new RuntimeException("no domain event payload found for " + aggregateId));
    }

    protected Optional<String> getFirstPayload(String table, UUID aggregateId) {
        return jdbc.queryForList(
                "SELECT PAYLOAD FROM " + schema + "." + table +
                        " WHERE AGGREGATE_IDENTIFIER = ? ORDER BY SEQUENCE_NUMBER ASC",
                new Object[] {aggregateId.toString()},
                String.class)
                .stream()
                .findFirst();
    }
}


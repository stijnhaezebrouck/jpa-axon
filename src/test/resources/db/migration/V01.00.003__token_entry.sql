CREATE TABLE TOKEN_ENTRY (
    PROCESSOR_NAME VARCHAR2(255 CHAR) NOT NULL,
	  SEGMENT NUMBER(10,0) NOT NULL,
	  OWNER VARCHAR2(255 CHAR),
	  TIMESTAMP VARCHAR2(255 CHAR) NOT NULL,
	  TOKEN BLOB,
	  TOKEN_TYPE VARCHAR2(255 CHAR),
	  PRIMARY KEY (PROCESSOR_NAME, SEGMENT));

comment on table TOKEN_ENTRY IS 'Axon table for eventtrackers';
comment on column TOKEN_ENTRY.PROCESSOR_NAME IS 'name of the processor';
comment on column TOKEN_ENTRY.SEGMENT IS 'segment number';
comment on column TOKEN_ENTRY.OWNER IS 'host/node that owns this token';
comment on column TOKEN_ENTRY.TIMESTAMP IS 'string representation of the timestamp for which events were already processed';
comment on column TOKEN_ENTRY.TOKEN IS 'token object';
comment on column TOKEN_ENTRY.TOKEN_TYPE IS 'type of token';

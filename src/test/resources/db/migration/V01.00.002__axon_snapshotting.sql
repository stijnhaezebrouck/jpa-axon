
CREATE TABLE SNAPSHOT_EVENT_ENTRY (
    AGGREGATE_IDENTIFIER VARCHAR2(255 CHAR) NOT NULL,
    SEQUENCE_NUMBER NUMBER(19,0) NOT NULL,
    TYPE VARCHAR2(255 CHAR) NOT NULL,
    EVENT_IDENTIFIER VARCHAR2(255 CHAR) NOT NULL,
    META_DATA CLOB,
    PAYLOAD_TYPE VARCHAR2(255 CHAR) NOT NULL,
    PAYLOAD_REVISION VARCHAR2(255 CHAR),
    PAYLOAD CLOB NOT NULL,
    TIME_STAMP VARCHAR2(255 CHAR) NOT NULL,
    PRIMARY KEY (AGGREGATE_IDENTIFIER, SEQUENCE_NUMBER, TYPE));

CREATE UNIQUE INDEX SNAPSHOT_EVENT_ENTRY_EVENT_UK ON SNAPSHOT_EVENT_ENTRY (EVENT_IDENTIFIER);

comment on table SNAPSHOT_EVENT_ENTRY IS 'Snapshot table. Master data of document component stored as snapshots (in case eventstore is too large)';
comment on column SNAPSHOT_EVENT_ENTRY.AGGREGATE_IDENTIFIER IS 'the aggregate from which this event originates';
comment on column SNAPSHOT_EVENT_ENTRY.SEQUENCE_NUMBER IS 'sequence number of this event within its aggregate';
comment on column SNAPSHOT_EVENT_ENTRY.TYPE IS 'the type of the aggregate the event is for';
comment on column SNAPSHOT_EVENT_ENTRY.EVENT_IDENTIFIER IS 'applicative event id';
comment on column SNAPSHOT_EVENT_ENTRY.META_DATA IS 'meta data for this event';
comment on column SNAPSHOT_EVENT_ENTRY.PAYLOAD IS 'the event serialized as json';
comment on column SNAPSHOT_EVENT_ENTRY.PAYLOAD_REVISION IS 'the version of the event';
comment on column SNAPSHOT_EVENT_ENTRY.PAYLOAD_TYPE IS 'the java class of the event type';
comment on column SNAPSHOT_EVENT_ENTRY.TIME_STAMP IS 'time at which event occured';

-- set the correct grants on the tables in the schema
BEGIN
  ops$oracle.grant_after_top.apply_new_grants ('SCH_AXON');
  ops$oracle.grant_after_top.apply_new_grants ('JENKINS_AXON');
END;
/

CREATE TABLE EVENT_LIST_VIEW (
  ID          NUMBER(19,0) NOT NULL ENABLE,
  AGGREGATEID VARCHAR2(255 CHAR),
  EVENTNAME   VARCHAR2(255 CHAR),
  PRIMARY KEY (ID)
);

comment on table EVENT_LIST_VIEW IS 'view showing all the events';
comment on column EVENT_LIST_VIEW.ID IS 'technical primary key';
comment on column EVENT_LIST_VIEW.AGGREGATEID IS 'the aggregate to which this event belongs';
comment on column EVENT_LIST_VIEW.EVENTNAME IS 'fully qualified name of the event class';

CREATE SEQUENCE VIEW_SEQ
    MINVALUE 1
    MAXVALUE 9999999999999999999999999999
    INCREMENT BY 1
    START WITH 1
    CACHE 20
    NOORDER NOCYCLE ;
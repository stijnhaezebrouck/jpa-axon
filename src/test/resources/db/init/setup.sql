BEGIN
  FOR USR IN (SElECT * FROM DBA_USERS WHERE username = 'OPS$ORACLE') LOOP
     execute immediate 'drop user ops$oracle cascade';
  END LOOP;
END;
/
-- ROLE SQL
BEGIN
 EXECUTE IMMEDIATE 'CREATE ROLE SESSION_CREALT';
EXCEPTION
 WHEN OTHERS THEN
   NULL;
END;
/

-- ROLES

-- SYSTEM PRIVILEGES
GRANT ALTER SESSION TO  SESSION_CREALT
/
GRANT CREATE SESSION TO  SESSION_CREALT
/

-- ROLE SQL
BEGIN
 EXECUTE IMMEDIATE 'CREATE ROLE SCHEMA_OWNER';
EXCEPTION
 WHEN OTHERS THEN
   NULL;
END;
/

-- ROLES

-- SYSTEM PRIVILEGES
GRANT CREATE TRIGGER TO  SCHEMA_OWNER
/
GRANT CREATE ANY DIRECTORY TO  SCHEMA_OWNER
/
GRANT CREATE MATERIALIZED VIEW TO  SCHEMA_OWNER
/
GRANT CREATE VIEW TO  SCHEMA_OWNER
/
GRANT CREATE TABLE TO  SCHEMA_OWNER
/
GRANT CREATE PUBLIC SYNONYM TO  SCHEMA_OWNER
/
GRANT CREATE SEQUENCE TO  SCHEMA_OWNER
/
GRANT DROP PUBLIC SYNONYM TO  SCHEMA_OWNER
/

-- USER SQL
CREATE USER  OPS$ORACLE IDENTIFIED BY oracle1
DEFAULT TABLESPACE USERS
TEMPORARY TABLESPACE TEMP
/

-- QUOTAS
ALTER USER OPS$ORACLE QUOTA UNLIMITED ON USERS
/

-- ROLES
GRANT CONNECT TO  OPS$ORACLE
/
GRANT RESOURCE TO  OPS$ORACLE
/
GRANT EXP_FULL_DATABASE TO  OPS$ORACLE
/
GRANT OEM_ADVISOR TO  OPS$ORACLE
/
GRANT SELECT_CATALOG_ROLE TO  OPS$ORACLE
/
GRANT SESSION_CREALT TO  OPS$ORACLE
/
ALTER USER OPS$ORACLE DEFAULT ROLE CONNECT,RESOURCE,EXP_FULL_DATABASE,OEM_ADVISOR,SELECT_CATALOG_ROLE,SESSION_CREALT
/

-- SYSTEM PRIVILEGES
GRANT CREATE JOB TO  OPS$ORACLE
/
GRANT ALTER ANY INDEX TO  OPS$ORACLE
/
GRANT ALTER SESSION TO  OPS$ORACLE
/
GRANT GRANT ANY OBJECT PRIVILEGE TO  OPS$ORACLE
/
GRANT ALTER ANY TABLE TO  OPS$ORACLE
/
GRANT CREATE SESSION TO  OPS$ORACLE
/
GRANT SELECT ANY TABLE TO  OPS$ORACLE
/
GRANT CREATE EXTERNAL JOB TO  OPS$ORACLE
/
GRANT CREATE TABLE TO  OPS$ORACLE
/
GRANT DROP ANY TABLE TO  OPS$ORACLE
/
GRANT ANALYZE ANY TO  OPS$ORACLE
/
GRANT SELECT ANY DICTIONARY TO  OPS$ORACLE
/
GRANT CREATE PUBLIC SYNONYM TO  OPS$ORACLE
/
GRANT RESTRICTED SESSION TO  OPS$ORACLE
/
GRANT ANALYZE ANY DICTIONARY TO  OPS$ORACLE
/
GRANT ALTER SYSTEM TO  OPS$ORACLE
/
GRANT UNLIMITED TABLESPACE TO  OPS$ORACLE
/
GRANT CREATE PROCEDURE TO  OPS$ORACLE
/


CREATE TABLE OPS$ORACLE.SCHEMA_ROLLEN
(	SCHEMA VARCHAR2(30 CHAR),
	ROLE VARCHAR2(30 CHAR),
	ROLE_TYPE VARCHAR2(4 CHAR)
)

/
ALTER TABLE OPS$ORACLE.SCHEMA_ROLLEN MODIFY (SCHEMA NOT NULL ENABLE)
/
ALTER TABLE OPS$ORACLE.SCHEMA_ROLLEN MODIFY (ROLE NOT NULL ENABLE)
/
ALTER TABLE OPS$ORACLE.SCHEMA_ROLLEN MODIFY (ROLE_TYPE NOT NULL ENABLE)
/


create or replace PACKAGE            OPS$ORACLE.GRANT_AFTER_TOP
IS
  PROCEDURE apply_new_grants (p_schema VARCHAR2 DEFAULT NULL,
                              p_object VARCHAR2 DEFAULT NULL);
END GRANT_AFTER_TOP;
/
create or replace PACKAGE BODY            OPS$ORACLE.GRANT_AFTER_TOP
IS
---------------------------------------------------------------------------------------------------------
-- This procedure is going to apply grants on new objects
-- If the parameter p_schema is filled in, only this schema is checked
-- If the parameter p_object is filled in, only this object is checked
-- If no parameter is filled in, every schema/object in the database is checked (of users in schema_rollen)
--
-- V1.0  09/08/2010  K. Vangeenderhuysen      Creation
---------------------------------------------------------------------------------------------------------
PROCEDURE apply_new_grants (p_schema VARCHAR2 DEFAULT NULL,
                            p_object VARCHAR2 DEFAULT NULL)
IS
  grant_option_does_not_exist EXCEPTION;
  PRAGMA EXCEPTION_INIT(grant_option_does_not_exist, -1720);

  CURSOR c_roles (cp_type   VARCHAR2,
                  cp_schema VARCHAR2)
  IS
    SELECT rs.role,
           rs.schema
      FROM ops$oracle.schema_rollen rs
     WHERE role_type = cp_type
       AND EXISTS (SELECT 1
                     FROM sys.dba_roles dr
                    WHERE dr.role = rs.role)
       AND rs.schema = nvl(cp_schema, rs.schema);

  CURSOR c_grants (cp_schema VARCHAR2,
                   cp_role   VARCHAR2,
                   cp_object VARCHAR2,
				   cp_role_type VARCHAR2,
				   cp_privilege VARCHAR2,
                   cp_type1  VARCHAR2,
                   cp_type2  VARCHAR2 DEFAULT NULL,
                   cp_type3  VARCHAR2 DEFAULT NULL,
                   cp_type4  VARCHAR2 DEFAULT NULL)
  IS
    SELECT owner,
           object_name,
           object_type
      FROM sys.dba_objects do
     WHERE owner = cp_schema
       AND object_type  IN (cp_type1, cp_type2, cp_type3, cp_type4)
       AND NOT EXISTS (SELECT 1
                                 FROM sys.dba_tab_privs
                                 WHERE table_name = do.object_name
                                 AND grantee = cp_role
         					     AND privilege = cp_privilege
					)
       AND object_name = nvl(cp_object, object_name)
       AND status = 'VALID'
     ORDER BY object_type, object_name;


BEGIN
  -- create all grants for all ro roles
  FOR r_ro IN c_roles (cp_type   => 'RO',
                       cp_schema => UPPER(p_schema))
  LOOP
    -- create select grants on all needed tables, views, sequences
    FOR r_gr IN c_grants (cp_schema => r_ro.schema,
                          cp_role   => r_ro.role,
						  cp_role_type => 'RO',
						  cp_privilege => 'SELECT',
                          cp_object => UPPER(p_object),
                          cp_type1  => 'TABLE',
                          cp_type2  => 'VIEW',
                          cp_type3  => 'SEQUENCE')
    LOOP
      BEGIN
        EXECUTE IMMEDIATE 'GRANT SELECT ON '||r_gr.owner||'."'||r_gr.object_name||'" TO '||r_ro.role;
      EXCEPTION
        -- op sommige views gaat dit niet lukken, raadplegen soms objecten uit andere schema's zoals sys
        WHEN grant_option_does_not_exist THEN
          dbms_output.put_line(SQLERRM);
        WHEN OTHERS THEN
          dbms_output.put_line('GRANT for RO on '||r_gr.owner||'.'||r_gr.object_name||' failed: '||SQLERRM);
      END;
    END LOOP; -- c_roles
  END LOOP; -- c_grants

  -- create all grants for all fb roles
  FOR r_fb IN c_roles (cp_type   => 'FB',
                      cp_schema => UPPER(p_schema))
  LOOP
    FOR r_gr IN c_grants (cp_schema => r_fb.schema,
                          cp_role   => r_fb.role,
						  cp_role_type => 'FB',
						  cp_privilege => 'FLASBACK',
                          cp_object => UPPER(p_object),
                          cp_type1  => 'TABLE',
                          cp_type2  => 'VIEW')
    LOOP
      BEGIN
        EXECUTE IMMEDIATE 'GRANT FLASHBACK ON '||r_gr.owner||'."'||r_gr.object_name||'" TO '||r_fb.role;
      EXCEPTION
        -- op sommige views gaat dit niet lukken, raadplegen soms objecten uit andere schema's zoals sys
        WHEN grant_option_does_not_exist THEN
          dbms_output.put_line(SQLERRM);
        WHEN OTHERS THEN
          dbms_output.put_line('GRANT for FB on '||r_gr.owner||'.'||r_gr.object_name||' failed: '||SQLERRM);
      END;
    END LOOP;

  END LOOP;

  -- create all necessary grants for all rw roles
  FOR r_rw IN c_roles (cp_type   => 'RW',
                       cp_schema => UPPER(p_schema))
  LOOP
    -- create select, insert, update, delete grants on all needed tables, views, sequences
    FOR r_gr IN c_grants (cp_schema => r_rw.schema,
                          cp_role   => r_rw.role,
						  cp_role_type => 'RW',
						  cp_privilege => 'UPDATE',
                          cp_object => UPPER(p_object),
                          cp_type1  => 'TABLE',
                          cp_type2  => 'VIEW',
                          cp_type3  => 'SEQUENCE')
    LOOP
      BEGIN
        IF r_gr.object_type = 'SEQUENCE'
        THEN
          EXECUTE IMMEDIATE 'GRANT SELECT ON '||r_gr.owner||'."'||r_gr.object_name||'" TO '||r_rw.role;
        ELSE
          EXECUTE IMMEDIATE 'GRANT SELECT, INSERT, UPDATE, DELETE ON '||r_gr.owner||'."'||r_gr.object_name||'" TO '||r_rw.role;
        END IF;
      EXCEPTION
        WHEN grant_option_does_not_exist THEN
          dbms_output.put_line(SQLERRM);
        WHEN OTHERS THEN
          dbms_output.put_line('GRANT for RW on '||r_gr.owner||'.'||r_gr.object_name||' failed: '||SQLERRM);
      END;
    END LOOP; -- c_roles
  END LOOP; -- c_grants

  -- create all necessary grants for all exec roles
  FOR r_exe IN c_roles (cp_type   => 'EXEC',
                        cp_schema => UPPER(p_schema))
  LOOP
    -- create exec grants on all needed functions, procedures, packages and types
    FOR r_gr IN c_grants (cp_schema => r_exe.schema,
                          cp_role   => r_exe.role,
                          cp_object => UPPER(p_object),
						  cp_role_type => 'EXEC',
						  cp_privilege => 'EXECUTE',
                          cp_type1  => 'FUNCTION',
                          cp_type2  => 'PROCEDURE',
                          cp_type3  => 'PACKAGE',
                          cp_type4  => 'TYPE')
    LOOP
      BEGIN
        EXECUTE IMMEDIATE 'GRANT EXECUTE ON '||r_gr.owner||'."'||r_gr.object_name||'" TO '||r_exe.role;
      EXCEPTION
        WHEN OTHERS THEN
          dbms_output.put_line('GRANT for EXEC on '||r_gr.owner||'.'||r_gr.object_name||' failed: '||SQLERRM);
      END;
    END LOOP; -- c_roles
  END LOOP; -- c_grants
  FOR r_gr IN (    SELECT owner,
                          object_name,
                          object_type
                   FROM sys.dba_objects do
                   WHERE owner NOT IN ('SYS','DBSNMP','CTXSYS','XDB','WMSYS','EXFSYS','SYSTEM','MDSYS')
                   AND owner NOT LIKE 'APEX%'
                   AND object_type  = 'TYPE'
                   AND object_name not like 'SYS%'
                   AND NOT EXISTS (SELECT 1
                                   FROM sys.dba_tab_privs
                                   WHERE table_name = do.object_name
                                   AND grantee = 'PUBLIC')
                   AND status = 'VALID'
                   ORDER BY owner, object_type, object_name
                   )
    LOOP
      BEGIN
        EXECUTE IMMEDIATE 'GRANT EXECUTE ON '||r_gr.owner||'."'||r_gr.object_name||'" TO PUBLIC';
      EXCEPTION
        WHEN OTHERS THEN
          dbms_output.put_line('GRANT for EXEC on '||r_gr.owner||'.'||r_gr.object_name||' failed: '||SQLERRM);
      END;
    END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Fout: '||SQLERRM);
END apply_new_grants;
---------------------------------------------------------------------------------------------------------
END GRANT_AFTER_TOP;
/
GRANT EXECUTE ON OPS$ORACLE.GRANT_AFTER_TOP TO PUBLIC
/

grant create tablespace to ops$oracle
/
grant create role to ops$oracle
/
grant create user to ops$oracle
/
grant grant any role to ops$oracle
/
grant alter user to ops$oracle
/
grant session_crealt to ops$oracle with admin option
/
grant schema_owner to ops$oracle with admin option
/

CREATE OR REPLACE PROCEDURE OPS$ORACLE.CREATE_TS_ROLE_SCH_POOL (
p_appl_abbr           IN VARCHAR2
,p_sch_pw             IN VARCHAR2
,p_pool_pw            IN VARCHAR2
,p_pw_encrypted       IN BOOLEAN DEFAULT FALSE
,p_debug_mode         IN BOOLEAN DEFAULT FALSE
)
 IS
l_owner VARCHAR2(20) := 'OPS$ORACLE';
l_stmt VARCHAR2(32000);
l_db_name VARCHAR2(30);

PROCEDURE EXEC_SHOW(p_stmt IN VARCHAR2) IS
BEGIN
  IF p_debug_mode THEN
     dbms_output.put_line(p_stmt);
  ELSE
    BEGIN
     EXECUTE IMMEDIATE p_stmt;
	EXCEPTION
	 when others then
	   dbms_output.put_line(p_stmt);
	   dbms_output.put_line(SQLERRM);
	END;
  END IF;
END EXEC_SHOW;
BEGIN
SELECT name into l_db_name from v$database;

EXEC_SHOW( 'CREATE ROLE '||upper(p_appl_abbr)||'_ROLE_RO');
EXEC_SHOW( 'REVOKE '||upper(p_appl_abbr)||'_ROLE_RO FROM '||l_owner);
EXEC_SHOW( 'CREATE ROLE '||upper(p_appl_abbr)||'_ROLE_RW');
EXEC_SHOW( 'REVOKE '||upper(p_appl_abbr)||'_ROLE_RW FROM '||l_owner);
EXEC_SHOW( 'CREATE ROLE '||upper(p_appl_abbr)||'_ROLE_EXEC');
EXEC_SHOW( 'REVOKE '||upper(p_appl_abbr)||'_ROLE_EXEC FROM '||l_owner);
EXEC_SHOW( 'CREATE ROLE '||upper(p_appl_abbr)||'_ROLE_FB');
EXEC_SHOW( 'REVOKE '||upper(p_appl_abbr)||'_ROLE_FB FROM '||l_owner);


delete from ops$oracle.schema_rollen where role = upper(p_appl_abbr)||'_ROLE_EXEC';

insert into ops$oracle.schema_rollen (schema, role, role_type) values ('SCH_'||upper(p_appl_abbr), upper(p_appl_abbr)||'_ROLE_EXEC', 'EXEC');

delete from ops$oracle.schema_rollen where role = upper(p_appl_abbr)||'_ROLE_FB';

insert into ops$oracle.schema_rollen   (schema, role, role_type) values ('SCH_'||upper(p_appl_abbr), upper(p_appl_abbr)||'_ROLE_FB', 'FB');

delete from ops$oracle.schema_rollen where role = upper(p_appl_abbr)||'_ROLE_RO';

insert into ops$oracle.schema_rollen   (schema, role, role_type) values ('SCH_'||upper(p_appl_abbr), upper(p_appl_abbr)||'_ROLE_RO', 'RO');

delete from ops$oracle.schema_rollen where role = upper(p_appl_abbr)||'_ROLE_RW';

insert into ops$oracle.schema_rollen  (schema, role, role_type) values ('SCH_'||upper(p_appl_abbr), upper(p_appl_abbr)||'_ROLE_RW', 'RW');

commit;
ops$oracle.grant_after_top.apply_new_grants;


l_stmt := 'CREATE USER  SCH_'||upper(p_appl_abbr);
IF p_pw_encrypted THEN
l_stmt :=  l_stmt || ' IDENTIFIED BY VALUES '''||p_sch_pw||'''';
ELSE
l_stmt :=  l_stmt || ' IDENTIFIED BY '||p_sch_pw;
END IF;
l_stmt :=  l_stmt || ' DEFAULT TABLESPACE USERS';
l_stmt :=  l_stmt || ' TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK';
EXEC_SHOW( l_stmt);

l_stmt := 'GRANT SCHEMA_OWNER TO SCH_'||upper(p_appl_abbr);
EXEC_SHOW( l_stmt);
l_stmt := 'GRANT SESSION_CREALT TO SCH_'||upper(p_appl_abbr);
EXEC_SHOW( l_stmt);
l_stmt := 'ALTER USER SCH_'||upper(p_appl_abbr)||' QUOTA UNLIMITED ON USERS';
EXEC_SHOW( l_stmt);


l_stmt := 'CREATE USER  POOL_'||upper(p_appl_abbr);
IF p_pw_encrypted THEN
l_stmt :=  l_stmt || ' IDENTIFIED BY VALUES '''||p_pool_pw||'''';
ELSE
l_stmt :=  l_stmt || ' IDENTIFIED BY '||p_pool_pw;
END IF;
l_stmt :=  l_stmt || ' DEFAULT TABLESPACE USERS TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK';
EXEC_SHOW( l_stmt);


l_stmt := 'GRANT '||upper(p_appl_abbr)||'_ROLE_RW TO POOL_'||upper(p_appl_abbr);
EXEC_SHOW( l_stmt);
l_stmt := 'GRANT '||upper(p_appl_abbr)||'_ROLE_EXEC TO POOL_'||upper(p_appl_abbr);
EXEC_SHOW( l_stmt);
l_stmt := 'GRANT SESSION_CREALT TO POOL_'||upper(p_appl_abbr);
EXEC_SHOW( l_stmt);
END;
/

begin
for rusr in (select username from dba_users where username like '%AXON%') loop
begin
 execute immediate 'drop user '|| rusr.username ||' cascade';
exception
 when others then
  null;
end;
end loop;
end;
/


begin
ops$oracle.create_ts_role_sch_pool('AXON','AXON','AXON');
end;
/

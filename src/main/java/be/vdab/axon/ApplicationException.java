package be.vdab.axon;

public class ApplicationException extends RuntimeException {

    public ApplicationException(String message) {
      super(message);
    }
}

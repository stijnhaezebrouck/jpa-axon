package be.vdab.axon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@EnableTransactionManagement
@SpringBootApplication
//@EntityScan({"org.axonframework.eventsourcing.eventstore.jpa",
//        "org.axonframework.eventhandling.saga.repository.jpa",
//        "org.axonframework.eventhandling.tokenstore.jpa",
//        "be.vdab.axon"})
public class Application {

    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }

}

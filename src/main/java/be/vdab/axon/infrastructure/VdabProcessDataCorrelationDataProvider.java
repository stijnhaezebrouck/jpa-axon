package be.vdab.axon.infrastructure;

import org.axonframework.messaging.Message;
import org.axonframework.messaging.correlation.CorrelationDataProvider;
import org.springframework.stereotype.Component;


import java.util.HashMap;
import java.util.Map;

import static be.vdab.axon.infrastructure.ProcessDataCommandInterceptor.VDAB_PROCESSDATA_HEADER;

@Component
public class VdabProcessDataCorrelationDataProvider implements CorrelationDataProvider {

    @Override
    public Map<String, Object> correlationDataFor(Message<?> message) {
        Map<String, Object> metadata = new HashMap<>();
        metadata.put(VDAB_PROCESSDATA_HEADER, message.getMetaData().get(VDAB_PROCESSDATA_HEADER));
        return metadata;
    }
}

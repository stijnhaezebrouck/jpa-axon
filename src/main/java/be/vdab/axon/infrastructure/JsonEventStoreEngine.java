package be.vdab.axon.infrastructure;

import org.axonframework.common.jdbc.PersistenceExceptionResolver;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.eventhandling.EventMessage;
import org.axonframework.eventsourcing.DomainEventMessage;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.axonframework.serialization.Serializer;
import org.axonframework.serialization.upcasting.event.EventUpcaster;

import static org.axonframework.eventsourcing.eventstore.EventUtils.asDomainEventMessage;

public class JsonEventStoreEngine extends JpaEventStorageEngine {

    public JsonEventStoreEngine(Serializer serializer, EventUpcaster upcasterChain,
                                PersistenceExceptionResolver persistenceExceptionResolver,
                                EntityManagerProvider entityManagerProvider,
                                TransactionManager transactionManager) {
        super(serializer,
            upcasterChain,
            persistenceExceptionResolver,
            serializer,
            null,
            entityManagerProvider,
            transactionManager,
            null,
            null, true);

    }

    @Override
    protected Object createEventEntity(EventMessage<?> eventMessage, Serializer serializer) {
        return new JsonDomainEventEntry(asDomainEventMessage(eventMessage), serializer);
    }

    @Override
    protected String domainEventEntryEntityName() {
        return JsonDomainEventEntry.class.getSimpleName();
    }

    @Override
    protected Object createSnapshotEntity(DomainEventMessage<?> snapshot, Serializer serializer) {
        return new JsonSnapshotEventEntry(snapshot, serializer);
    }

    @Override
    protected String snapshotEventEntryEntityName() {
        return JsonSnapshotEventEntry.class.getSimpleName();
    }
}

package be.vdab.axon.infrastructure;

import org.axonframework.eventsourcing.DomainEventMessage;
import org.axonframework.eventsourcing.eventstore.AbstractDomainEventEntry;
import org.axonframework.eventsourcing.eventstore.AbstractSequencedDomainEventEntry;
import org.axonframework.serialization.Serializer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = JsonDomainEventEntry.TABLE_NAME, indexes = @Index(columnList = "aggregateIdentifier,sequenceNumber", unique = true))
@SequenceGenerator(name="mijnDomainEventGenerator", sequenceName = "DOMAIN_EVENT_ENTRY_SEQ", allocationSize = 1)
public class JsonDomainEventEntry extends AbstractDomainEventEntry<String> {
    public static final String TABLE_NAME = "DOMAIN_EVENT_ENTRY";

    @Id
    @GeneratedValue(generator="mijnDomainEventGenerator",strategy = SEQUENCE)
    @SuppressWarnings("unused")
    private long globalIndex;

    public JsonDomainEventEntry(DomainEventMessage<?> domainEventMessage, Serializer serializer) {
        super(domainEventMessage, serializer, String.class);
    }

    protected JsonDomainEventEntry() {
        //JPA requirement
    }
}

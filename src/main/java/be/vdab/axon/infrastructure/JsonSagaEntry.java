package be.vdab.axon.infrastructure;

import org.axonframework.eventhandling.saga.repository.jpa.AbstractSagaEntry;
import org.axonframework.serialization.Serializer;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = JsonSagaEntry.TABLE_NAME)
public class JsonSagaEntry extends AbstractSagaEntry<String> {
    public static final String TABLE_NAME = "SAGA_ENTRY";

    public JsonSagaEntry(Object saga, String sagaIdentifier, Serializer serializer) {
        super(saga, sagaIdentifier, serializer, String.class);
    }

    protected JsonSagaEntry(){
        //JPA requirement
    }
}

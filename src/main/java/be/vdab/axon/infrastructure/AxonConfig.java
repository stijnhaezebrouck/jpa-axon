package be.vdab.axon.infrastructure;

import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.common.jdbc.PersistenceExceptionResolver;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.config.EventHandlingConfiguration;
import org.axonframework.eventhandling.saga.repository.SagaStore;
import org.axonframework.eventsourcing.EventCountSnapshotTriggerDefinition;
import org.axonframework.eventsourcing.NoSnapshotTriggerDefinition;
import org.axonframework.eventsourcing.SnapshotTriggerDefinition;
import org.axonframework.eventsourcing.Snapshotter;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.messaging.annotation.ParameterResolverFactory;
import org.axonframework.serialization.Serializer;
import org.axonframework.serialization.upcasting.event.EventUpcaster;
import org.axonframework.spring.config.AxonConfiguration;
import org.axonframework.spring.eventhandling.scheduling.quartz.QuartzEventSchedulerFactoryBean;
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SimpleThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.time.Clock;
import java.util.concurrent.Executor;

@Configuration
public class AxonConfig {

    @Value("${axon.snapshot.threads:1}")
    private int numberOfAxonThreads;

    @Value("${axon.snapshot.threshold:3}")
    private int axonSnapshotThreshold;

    public int snapshotThreshold() {
        return axonSnapshotThreshold;
    }


    @Autowired
    public void useEventTrackingProcessors(EventHandlingConfiguration eventHandlingConfiguration) {
        //eventHandlingConfiguration.usingTrackingProcessors();
    }

    @Bean
    public QuartzEventSchedulerFactoryBean quartzEventSchedulerFactoryBean(PlatformTransactionManager txManager) {
        QuartzEventSchedulerFactoryBean quartzEventSchedulerFactoryBean = new QuartzEventSchedulerFactoryBean();
        quartzEventSchedulerFactoryBean.setTransactionManager(txManager);
        quartzEventSchedulerFactoryBean.setTransactionDefinition(new DefaultTransactionDefinition());
        return quartzEventSchedulerFactoryBean;
    }

    @Bean
    public Executor axonThreadPool() {
        SimpleThreadPoolTaskExecutor executor = new SimpleThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("snapshotThreadPool");
        executor.setThreadCount(numberOfAxonThreads);
        return executor;
    }

    @Bean
    public Snapshotter snapshotter(EventStore eventStore, ParameterResolverFactory parameterResolverFactory, Executor executor, TransactionManager txManager) {
        return new SpringAggregateSnapshotter(eventStore, parameterResolverFactory, executor, txManager);
    }

    @Bean
    public SnapshotTriggerDefinition createSnapshotTriggerDefinition(Snapshotter snapshotter) {
        if (snapshotThreshold() <= 0) {
            return NoSnapshotTriggerDefinition.INSTANCE;
        }
        return new EventCountSnapshotTriggerDefinition(snapshotter, snapshotThreshold());
    }

    @Bean
    public EventStorageEngine eventStorageEngine(
            Serializer serializer,
            PersistenceExceptionResolver persistenceExceptionResolver,
            AxonConfiguration configuration,
            EntityManagerProvider entityManagerProvider,
            TransactionManager transactionManager) {
        return new JsonEventStoreEngine(serializer,
                configuration.getComponent(EventUpcaster.class),
                persistenceExceptionResolver,
                entityManagerProvider,
                transactionManager);
    }

    @Bean
    public SagaStore<Object> sagaStore(Serializer serializer, EntityManagerProvider entityManagerProvider){
        return new JsonSagaStore(serializer, entityManagerProvider);
    }

    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }

    @Autowired
    public void registerInterceptors(SimpleCommandBus commandBus, ProcessDataCommandInterceptor processDataCommandInterceptor) {
        commandBus.registerDispatchInterceptor(processDataCommandInterceptor);
    }
}

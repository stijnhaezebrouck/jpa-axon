package be.vdab.axon.infrastructure;

import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.eventhandling.saga.repository.jpa.AbstractSagaEntry;
import org.axonframework.eventhandling.saga.repository.jpa.JpaSagaStore;
import org.axonframework.serialization.Serializer;
import org.axonframework.serialization.SimpleSerializedObject;

public class JsonSagaStore extends JpaSagaStore {

    public JsonSagaStore(Serializer serializer, EntityManagerProvider entityManagerProvider) {
        super(serializer, entityManagerProvider);
    }

    @Override
    protected AbstractSagaEntry<?> createSagaEntry(Object saga, String sagaIdentifier, Serializer serializer) {
        return new JsonSagaEntry(saga, sagaIdentifier, serializer);
    }

    @Override
    protected String sagaEntryEntityName() {
        return JsonSagaEntry.class.getSimpleName();
    }

    @Override
    protected Class<? extends SimpleSerializedObject<?>> serializedObjectType() {
        return JsonSerializedSaga.class;
    }
}

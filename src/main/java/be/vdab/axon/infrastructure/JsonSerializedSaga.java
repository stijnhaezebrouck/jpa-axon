package be.vdab.axon.infrastructure;

import org.axonframework.serialization.SimpleSerializedObject;

public class JsonSerializedSaga extends SimpleSerializedObject<String> {

    public JsonSerializedSaga(String data, String type, String revision) {
        super(data, String.class, type, revision);
    }
}

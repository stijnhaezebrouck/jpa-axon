package be.vdab.axon.infrastructure;

import be.vdab.axon.infrastructure.vdab.ProcessData;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

@Component
public class ProcessDataCommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {

    public static final String VDAB_PROCESSDATA_HEADER = "vdabProcessMetadata";

    @Autowired
    private Clock clock;

    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(List<CommandMessage<?>> messages) {
        return (messageIndex, message)-> message.andMetaData(generateProcessMetadata());
    }

    private Map<String, ?> generateProcessMetadata(){
        Map<String, Object> processMetadata = new HashMap<>();
        processMetadata.put(VDAB_PROCESSDATA_HEADER, ProcessData.build(LocalDateTime.now(clock)));
        return processMetadata;
    }
}

package be.vdab.axon.infrastructure.vdab;

import be.vdab.logstash.AuthorizationId;
import be.vdab.logstash.ProcessId;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ProcessData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String authorization;
    private String processId;
    private LocalDateTime eventTime;

    public static ProcessData build(LocalDateTime eventTime) {
        String authorization = AuthorizationId.getVdabAuthorization();
        String processId = ProcessId.getVdabProcessId();
        return new ProcessData(authorization, processId, eventTime);
    }

    private ProcessData(@JsonProperty("authorization") String authorization,
                        @JsonProperty("processId") String processId,
                        @JsonProperty("eventTime") LocalDateTime eventTime) {
        this.authorization = authorization;
        this.processId = processId;
        this.eventTime = eventTime;
    }

    public String getAuthorization() {
        return authorization;
    }

    public String getProcessId() {
        return processId;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }
}

package be.vdab.axon.eventview;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name="event_list_view")
public class EventListView {

    @Id @GeneratedValue(strategy = SEQUENCE, generator = "VIEW_SEQ_GEN")
    @SequenceGenerator(name="VIEW_SEQ_GEN", sequenceName="VIEW_SEQ")
    private long id;

    @Column(name="aggregateid")
    public String aggregateId;

    @Column(name="eventname")
    public String eventName;
}

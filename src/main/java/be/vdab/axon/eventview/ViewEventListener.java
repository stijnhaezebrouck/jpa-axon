package be.vdab.axon.eventview;

import be.vdab.axon.domain.aggregate.AggregateCreatedEvent;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringEventHandlerWithNewTransactionEvent;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringEventHandlerWithParticipatingTransactionEvent;
import be.vdab.axon.domain.aggregate.ThrowedExceptionDuringEventHandlerWithoutTransactionEvent;
import be.vdab.axon.ApplicationException;
import be.vdab.axon.Event;
import org.axonframework.eventhandling.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.UUID;

import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Component
public class ViewEventListener {

    private Logger log = LoggerFactory.getLogger(getClass());

    @PersistenceContext
    private EntityManager entityManager;

    @EventHandler
    public void on(AggregateCreatedEvent event) {
        insert(event.getAggregateId(), event.getClass());
    }

    @EventHandler
    public void genericOn(Event event) {
        insert(event.getAggregateId(), event.getClass());
    }

    @EventHandler
    @Transactional(propagation = REQUIRES_NEW)
    public void on(ThrowedExceptionDuringEventHandlerWithNewTransactionEvent event) throws InterruptedException {
        insert(event.getAggregateId(), event.getClass());
        throw new ApplicationException("exception from eventHandler");
    }

    @EventHandler
    @Transactional
    public void on(ThrowedExceptionDuringEventHandlerWithParticipatingTransactionEvent event) throws InterruptedException {
        insert(event.getAggregateId(), event.getClass());
        throw new ApplicationException("exception from eventHandler");
    }

    @EventHandler
    public void on(ThrowedExceptionDuringEventHandlerWithoutTransactionEvent event) throws InterruptedException {
        insert(event.getAggregateId(), event.getClass());
        throw new ApplicationException("exception from eventHandler");
    }

    private void insert(UUID id, Class<?> eventClass) {
        log.warn("adding event to view: {}", eventClass.getName());
        EventListView entity = new EventListView();
        entity.aggregateId = id.toString();
        entity.eventName = eventClass.getSimpleName();
        entityManager.persist(entity);
    }

}

package be.vdab.axon;

import java.util.UUID;

public interface Event {

    UUID getAggregateId();
}

package be.vdab.axon.domain.aggregate;

import org.axonframework.commandhandling.TargetAggregateIdentifier;
import org.slf4j.Logger;

import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

public class ThrowSagaExceptionCommand {
    private static final Logger LOGGER = getLogger(ThrowSagaExceptionCommand.class);

    @TargetAggregateIdentifier
    private final UUID id;

    public ThrowSagaExceptionCommand(UUID id) {
        LOGGER.info("{} aangemaakt", ThrowSagaExceptionCommand.class.getSimpleName());
        this.id = id;
    }


    public UUID getAggregateId() {
        return id;
    }
}

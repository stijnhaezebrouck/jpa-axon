package be.vdab.axon.domain.aggregate;

import be.vdab.axon.Event;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class ThrowedExceptionDuringEventHandlerWithoutTransactionEvent implements Event {
    private final UUID aggregateId;

    public ThrowedExceptionDuringEventHandlerWithoutTransactionEvent(@JsonProperty("aggregateId") UUID aggregateId) {

        this.aggregateId = aggregateId;
    }

    @Override
    public UUID getAggregateId() {
        return aggregateId;
    }

}

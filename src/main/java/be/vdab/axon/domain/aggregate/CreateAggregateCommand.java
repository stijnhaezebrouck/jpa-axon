package be.vdab.axon.domain.aggregate;

import java.util.UUID;

public class CreateAggregateCommand {

    private UUID aggregateId = UUID.randomUUID();


    public UUID getAggregateId() {
        return aggregateId;
    }
}

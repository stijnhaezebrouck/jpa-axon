package be.vdab.axon.domain.aggregate;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.util.UUID;

public class DoNothingCommand {

    @TargetAggregateIdentifier
    private final UUID id;

    public DoNothingCommand(UUID id) {
        this.id = id;
    }


    public UUID getAggregateId() {
        return id;
    }
}

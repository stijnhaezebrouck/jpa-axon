package be.vdab.axon.domain.aggregate;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.util.UUID;

public class ThrowExceptionDuringEventHandlerWithoutTransactionCommand {

    @TargetAggregateIdentifier
    private final UUID id;

    public ThrowExceptionDuringEventHandlerWithoutTransactionCommand(UUID id) {
        this.id = id;
    }


    public UUID getAggregateId() {
        return id;
    }
}

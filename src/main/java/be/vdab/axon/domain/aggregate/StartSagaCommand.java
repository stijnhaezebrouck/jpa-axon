package be.vdab.axon.domain.aggregate;

import org.axonframework.commandhandling.TargetAggregateIdentifier;
import org.slf4j.Logger;

import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

public class StartSagaCommand {
    private static final Logger LOGGER = getLogger(StartSagaCommand.class);

    @TargetAggregateIdentifier
    private final UUID id;

    public StartSagaCommand(UUID id) {
        this.id = id;
    }


    public UUID getAggregateId() {
        return id;
    }
}

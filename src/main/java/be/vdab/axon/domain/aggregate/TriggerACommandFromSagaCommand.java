package be.vdab.axon.domain.aggregate;

import org.axonframework.commandhandling.TargetAggregateIdentifier;
import org.slf4j.Logger;

import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

public class TriggerACommandFromSagaCommand {
    private static final Logger LOGGER = getLogger(TriggerACommandFromSagaCommand.class);

    @TargetAggregateIdentifier
    private final UUID id;
    private final Class<?> commandFromSaga;

    public TriggerACommandFromSagaCommand(UUID id, Class<?> commandFromSaga) {
        this.commandFromSaga = commandFromSaga;
        LOGGER.info("{} aangemaakt", TriggerACommandFromSagaCommand.class.getSimpleName());
        this.id = id;
    }


    public UUID getAggregateId() {
        return id;
    }

    public Class<?> getCommandClass() {
        return this.commandFromSaga;
    }
}

package be.vdab.axon.domain.aggregate;

import be.vdab.axon.Event;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class SagaExpiredEvent implements Event, Serializable {

    private static final long serialVersionUID = 1L;

    private UUID aggregateId;

    public SagaExpiredEvent(@JsonProperty("aggregateId") UUID aggregateId) {
        this.aggregateId = aggregateId;
    }

    @Override
    public UUID getAggregateId() {
        return aggregateId;
    }
}

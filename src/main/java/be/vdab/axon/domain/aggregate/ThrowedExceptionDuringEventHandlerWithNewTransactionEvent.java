package be.vdab.axon.domain.aggregate;

import be.vdab.axon.Event;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class ThrowedExceptionDuringEventHandlerWithNewTransactionEvent implements Event {
    private final UUID aggregateId;

    public ThrowedExceptionDuringEventHandlerWithNewTransactionEvent(@JsonProperty("aggregateId") UUID aggregateId) {

        this.aggregateId = aggregateId;
    }

    @Override
    public UUID getAggregateId() {
        return aggregateId;
    }

}

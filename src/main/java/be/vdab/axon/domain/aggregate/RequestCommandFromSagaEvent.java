package be.vdab.axon.domain.aggregate;

import be.vdab.axon.Event;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.axonframework.commandhandling.TargetAggregateIdentifier;
import org.slf4j.Logger;

import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

public class RequestCommandFromSagaEvent implements Event {
    private static final Logger LOGGER = getLogger(RequestCommandFromSagaEvent.class);

    @TargetAggregateIdentifier
    private final UUID id;
    private final Class<?> commandClass;

    public RequestCommandFromSagaEvent(@JsonProperty("aggregateId") UUID id, @JsonProperty("commandClass") Class<?> commandClass) {
        this.commandClass = commandClass;
        LOGGER.info("{} aangemaakt", RequestCommandFromSagaEvent.class.getSimpleName());
        this.id = id;
    }

    @Override
    public UUID getAggregateId() {
        return id;
    }

    public Class<?> getCommandClass() {
        return commandClass;
    }
}

package be.vdab.axon.domain.aggregate;

import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.SnapshotTriggerDefinition;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationAggregateRepository extends EventSourcingRepository<ApplicationAggregate> {

    public ApplicationAggregateRepository(@Autowired EventStore eventStore, @Autowired SnapshotTriggerDefinition snapshotTriggerDefinition) {
        super(ApplicationAggregate.class, eventStore, snapshotTriggerDefinition);
    }
}

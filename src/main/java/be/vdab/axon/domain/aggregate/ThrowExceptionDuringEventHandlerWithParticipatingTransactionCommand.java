package be.vdab.axon.domain.aggregate;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.util.UUID;

public class ThrowExceptionDuringEventHandlerWithParticipatingTransactionCommand {

    @TargetAggregateIdentifier
    private final UUID id;

    public ThrowExceptionDuringEventHandlerWithParticipatingTransactionCommand(UUID id) {
        this.id = id;
    }


    public UUID getAggregateId() {
        return id;
    }
}

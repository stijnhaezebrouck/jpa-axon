package be.vdab.axon.domain.aggregate;

import be.vdab.axon.Event;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;

import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

public class SagaStartedEvent implements Event {
    private static final Logger LOGGER = getLogger(SagaStartedEvent.class);

    private final UUID aggregateId;
    private final String sagaState;

    public SagaStartedEvent(@JsonProperty("aggregateId") UUID aggregateId, @JsonProperty("sagaState") String sagaState) {
        this.sagaState = sagaState;
        LOGGER.info("{} aangemaakt", SagaStartedEvent.class.getSimpleName());
        this.aggregateId = aggregateId;
    }

    @Override
    public UUID getAggregateId() {
        return aggregateId;
    }

    public String getSagaState() {
        return sagaState;
    }
}

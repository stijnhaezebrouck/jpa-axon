package be.vdab.axon.domain.aggregate;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.util.UUID;

public class ThrowExceptionDuringCommandHandler {

    @TargetAggregateIdentifier
    private final UUID id;

    public ThrowExceptionDuringCommandHandler(UUID id) {
        this.id = id;
    }

    public UUID getAggregateId() {
        return id;
    }
}

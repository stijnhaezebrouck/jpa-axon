package be.vdab.axon.domain.aggregate;

import be.vdab.axon.Event;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class SagaExceptionRequestedEvent implements Event {
    private final UUID aggregateId;
    private final String sagaState;

    public SagaExceptionRequestedEvent(@JsonProperty("aggregateId") UUID aggregateId, @JsonProperty("sagaState") String sagaState) {
        this.aggregateId = aggregateId;
        this.sagaState = sagaState;
    }

    @Override
    public UUID getAggregateId() {
        return aggregateId;
    }

    public String getSagaState() {
        return sagaState;
    }
}

package be.vdab.axon.domain.aggregate;

import be.vdab.axon.Event;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class ThrowedExceptionDuringCommandHandlerEvent implements Event {
    private final UUID aggregateId;

    public ThrowedExceptionDuringCommandHandlerEvent(@JsonProperty("aggregateId") UUID aggregateId) {
        this.aggregateId = aggregateId;
    }

    @Override
    public UUID getAggregateId() {
        return aggregateId;
    }
}

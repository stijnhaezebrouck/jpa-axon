package be.vdab.axon.domain.aggregate;

import be.vdab.axon.ApplicationException;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.slf4j.Logger;

import java.util.UUID;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;
import static org.slf4j.LoggerFactory.getLogger;

@Aggregate
public class ApplicationAggregate {

    private static final Logger LOGGER = getLogger(ApplicationAggregate.class);

    @AggregateIdentifier
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("sagaCommandCounter")
    private int sagaCommandCounter;

    public ApplicationAggregate() {}

    protected ApplicationAggregate(@JsonProperty("id") UUID id, @JsonProperty("sagaCommandCounter") int sagaCommandCounter) {
        this.id = id;
        this.sagaCommandCounter = sagaCommandCounter;
    }

    @CommandHandler
    public ApplicationAggregate(CreateAggregateCommand cmd) {
        apply(new AggregateCreatedEvent(cmd.getAggregateId()));
    }

    @EventSourcingHandler
    private void on(AggregateCreatedEvent event) {
        this.id = event.getAggregateId();
        this.sagaCommandCounter = 1;
    }

    @CommandHandler
    public void handle(ThrowExceptionDuringCommandHandler cmd) {
        apply(new ThrowedExceptionDuringCommandHandlerEvent(cmd.getAggregateId()));
        LOGGER.info("handle command: {}", cmd.getClass().getSimpleName());
        throw new ApplicationException("exception from command handler in aggregate root");
    }

    @CommandHandler
    public  void handle(ThrowExceptionDuringEventHandlerWithNewTransactionCommand cmd) {
        apply(new ThrowedExceptionDuringEventHandlerWithNewTransactionEvent(cmd.getAggregateId()));
    }

    @CommandHandler
    public  void handle(ThrowExceptionDuringEventHandlerWithParticipatingTransactionCommand cmd) {
        apply(new ThrowedExceptionDuringEventHandlerWithParticipatingTransactionEvent(cmd.getAggregateId()));
    }

    @CommandHandler
    public void handle(ThrowExceptionDuringEventHandlerWithoutTransactionCommand cmd) {
        apply(new ThrowedExceptionDuringEventHandlerWithoutTransactionEvent(cmd.getAggregateId()));
    }

    @CommandHandler
    public void handle(StartSagaCommand cmd) {
        apply(new SagaStartedEvent(cmd.getAggregateId(), "saga " + sagaCommandCounter));
    }

    @EventSourcingHandler
    private void on(SagaStartedEvent e) {
        sagaCommandCounter++;
    }

    @CommandHandler
    public void handle(ThrowSagaExceptionCommand cmd) {
        apply(new SagaExceptionRequestedEvent(cmd.getAggregateId(), "saga " + sagaCommandCounter));
    }

    @EventSourcingHandler
    private void on(SagaExceptionRequestedEvent e) {
        sagaCommandCounter++;
    }

    @CommandHandler
    public void handle(TriggerACommandFromSagaCommand cmd) {
        apply(new RequestCommandFromSagaEvent(cmd.getAggregateId(), cmd.getCommandClass()));
    }

    @CommandHandler
    public void handle(DoNothingCommand cmd) {
        apply(new NothingDoneEvent(cmd.getAggregateId()));
    }
}

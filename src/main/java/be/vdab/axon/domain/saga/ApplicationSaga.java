package be.vdab.axon.domain.saga;

import be.vdab.axon.domain.aggregate.RequestCommandFromSagaEvent;
import be.vdab.axon.domain.aggregate.SagaExceptionRequestedEvent;
import be.vdab.axon.domain.aggregate.SagaExpiredEvent;
import be.vdab.axon.domain.aggregate.SagaStartedEvent;
import be.vdab.axon.ApplicationException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventhandling.saga.EndSaga;
import org.axonframework.eventhandling.saga.SagaEventHandler;
import org.axonframework.eventhandling.saga.StartSaga;
import org.axonframework.eventhandling.scheduling.EventScheduler;
import org.axonframework.messaging.MetaData;
import org.axonframework.spring.stereotype.Saga;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Constructor;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

@Saga
public class ApplicationSaga {

    private static final Logger LOGGER = getLogger(ApplicationSaga.class);

    @Autowired
    private transient CommandGateway commandGateway;

    @Autowired
    private transient EventScheduler eventScheduler;

    private String sagaState;

    @StartSaga
    @SagaEventHandler(associationProperty = "aggregateId")
    public void on(SagaStartedEvent event, MetaData metaData) {
        LOGGER.info("Saga gestart");
        sagaState = event.getSagaState();
        eventScheduler.schedule(Duration.of(5, ChronoUnit.SECONDS), new SagaExpiredEvent(event.getAggregateId()));
    }

    @SagaEventHandler(associationProperty = "aggregateId")
    public void on(SagaExceptionRequestedEvent event) {
        LOGGER.info("SagaException requested");
        sagaState = event.getSagaState();
        throw new ApplicationException("thrown from saga event handler");
    }

    @SagaEventHandler(associationProperty = "aggregateId")
    public void on(RequestCommandFromSagaEvent event) throws Exception {
        Class<?> commandClass = event.getCommandClass();
        Constructor<?> constructor = commandClass.getConstructor(UUID.class);
        Object commandToExecute = constructor.newInstance(event.getAggregateId());

       commandGateway.sendAndWait(commandToExecute);
    }

    @EndSaga
    @SagaEventHandler(associationProperty = "aggregateId")
    public void on(SagaExpiredEvent sagaExpiredEvent) {
        LOGGER.info("SagaExpiredEvent ontvanger");
    }
}
